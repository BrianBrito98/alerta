package com.example.alerta;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alerta.Api.Api;
import com.example.alerta.Viewmodels.AdapterV;
import com.example.alerta.Viewmodels.Peticion_Visualizaciones;
import com.example.alerta.Api.Service.ServicioPeticion;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Visualizaciones extends AppCompatActivity {

    ServicioPeticion servicio;
    RecyclerView rvVisualizacones;
    AdapterV adapterV;
    Button btnRecargar;
    ImageButton imgbAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizaciones);
        servicio = Api.getApi(Visualizaciones.this).create(ServicioPeticion.class);
        adapterV = new AdapterV();
        rvVisualizacones = findViewById(R.id.rvVisu);
        rvVisualizacones.setHasFixedSize(true);
        rvVisualizacones.setLayoutManager(new LinearLayoutManager(Visualizaciones.this));
        rvVisualizacones.addItemDecoration(new DividerItemDecoration(Visualizaciones.this, DividerItemDecoration.VERTICAL));
        TodasVisualizaciones();
    }

    private void TodasVisualizaciones(){
        servicio.Visualizaciones("s").enqueue(new Callback<Peticion_Visualizaciones>() {
            @Override
            public void onResponse(Call<Peticion_Visualizaciones> call, Response<Peticion_Visualizaciones> response) {
                if(response.isSuccessful()){
                    List<Peticion_Visualizaciones.detalle> visualizaciones = response.body().getVisualizaciones();
                    adapterV.setData(visualizaciones);
                    rvVisualizacones.setAdapter(adapterV);
                }else{
                    Toast.makeText(Visualizaciones.this, "Error procesando al usuario", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Peticion_Visualizaciones> call, Throwable t) {
                Toast.makeText(Visualizaciones.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}