package com.example.alerta;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alerta.Api.Api;
import com.example.alerta.Viewmodels.AdapterA;
import com.example.alerta.Viewmodels.Peticion_Alertas;
import com.example.alerta.Api.Service.ServicioPeticion;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Alertas extends AppCompatActivity {

    ServicioPeticion servicio;
    RecyclerView rvAlertas;
    AdapterA adapterA;
    Button btnRecargar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas);
        servicio = Api.getApi(Alertas.this).create(ServicioPeticion.class);
        adapterA = new AdapterA();
        rvAlertas = findViewById(R.id.rvAlerts);
        rvAlertas.setHasFixedSize(true);
        rvAlertas.setLayoutManager(new LinearLayoutManager(Alertas.this));
        rvAlertas.addItemDecoration(new DividerItemDecoration(Alertas.this, DividerItemDecoration.VERTICAL));
        AlertasR();
    }

    public void AlertasR(){
        servicio.Alertas("s").enqueue(new Callback<Peticion_Alertas>() {
            @Override
            public void onResponse(Call<Peticion_Alertas> call, Response<Peticion_Alertas> response) {
                if (response.isSuccessful()) {
                    List<Peticion_Alertas.detalle> alertas = response.body().getAlertas();
                    adapterA.setData(alertas);
                    rvAlertas.setAdapter(adapterA);
                } else {
                    Log.e("Api Error: ", response.message() + " errorBody:" + response.errorBody());
                    Toast.makeText(Alertas.this, "A Ocurrido un error al procesar a los usuarios", Toast.LENGTH_LONG).show();
                    AlertasR();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Alertas> call, Throwable t) {
                Log.e("Api Error: ", t.getMessage() +" "+ t.getCause());
                Toast.makeText(Alertas.this, "A Ocurrido Al conectarse con el servidor", Toast.LENGTH_LONG).show();
            }
        });
    }

}