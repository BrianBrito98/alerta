package com.example.alerta;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alerta.Api.Api;
import com.example.alerta.Viewmodels.AdapterV;
import com.example.alerta.Viewmodels.Peticion_Visualizaciones;
import com.example.alerta.Api.Service.ServicioPeticion;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisualizacionU extends AppCompatActivity {

    ServicioPeticion servicio;
    RecyclerView rvVisualizaconesU;
    AdapterV adapterV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizacion_u);
        servicio = Api.getApi(VisualizacionU.this).create(ServicioPeticion.class);
        adapterV = new AdapterV();
        rvVisualizaconesU = findViewById(R.id.rvVisuU);
        rvVisualizaconesU.setHasFixedSize(true);
        rvVisualizaconesU.setLayoutManager(new LinearLayoutManager(VisualizacionU.this));
        rvVisualizaconesU.addItemDecoration(new DividerItemDecoration(VisualizacionU.this, DividerItemDecoration.VERTICAL));
        VisualizacionesUsuario();
    }

    private void VisualizacionesUsuario(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio.VisualizacionesU(preferencias.getString("ID", "")).enqueue(new Callback<Peticion_Visualizaciones>() {
            @Override
            public void onResponse(Call<Peticion_Visualizaciones> call, Response<Peticion_Visualizaciones> response) {
                if(response.isSuccessful()){
                    if(!response.body().getVisualizaciones().isEmpty() || response.body().isEstado()){
                        List<Peticion_Visualizaciones.detalle> visualizaciones = response.body().getVisualizaciones();
                        adapterV.setData(visualizaciones);
                        rvVisualizaconesU.setAdapter(adapterV);
                    }else{
                        Toast.makeText(VisualizacionU.this, "No se encontraron los datos", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(VisualizacionU.this, "Ocurrio un error al procesar a los usuarios", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Visualizaciones> call, Throwable t) {
                Toast.makeText(VisualizacionU.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}