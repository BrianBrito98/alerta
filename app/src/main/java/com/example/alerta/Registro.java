package com.example.alerta;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.alerta.Api.Api;
import com.example.alerta.Viewmodels.Peticion_Registro;
import com.example.alerta.Api.Service.ServicioPeticion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    Button btnRegistro;
    ServicioPeticion service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        service = Api.getApi(Registro.this).create(ServicioPeticion.class);
        btnRegistro = findViewById(R.id.btnRegistrar);
        final EditText edtCorreo = findViewById(R.id.edtEmail);
        final EditText edtContra = findViewById(R.id.edtPassword);
        final EditText edtContra2 = findViewById(R.id.edtPasswordRep);
        final TextView tvRegresar = findViewById(R.id.txtvRegresar);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edtContra.getText().toString().equals(edtContra2.getText().toString()))
                    Toast.makeText(Registro.this,"Las contraseñas no coinciden",Toast.LENGTH_LONG).show();

                if(edtCorreo.getText().toString().equals("") && edtContra.getText().toString().equals("") && edtContra2.getText().toString().equals("")){
                    Toast.makeText(Registro.this,"Llene los campos",Toast.LENGTH_LONG).show();
                }else
                    RegistrarUsuario(edtCorreo.getText().toString(), edtContra.getText().toString());
            }
        });
        tvRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Registro.this, MainActivity.class));
                finish();
            }
        });
    }
    public void RegistrarUsuario(String Correo, String Password){
        Call<Peticion_Registro> callregistro = service.registroU(Correo, Password);
        callregistro.enqueue(new Callback<Peticion_Registro>() {
            @Override
            public void onResponse(Call<Peticion_Registro> call, Response<Peticion_Registro> response) {
                Peticion_Registro peticion = response.body();
                if(response.isSuccessful()){
                    if(response.body() != null){
                        if(peticion.estado == "true"){
                            startActivity(new Intent(Registro.this, MainActivity.class));
                            finish();
                        } else{
                            Toast.makeText(Registro.this,peticion.detalle,Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(Registro.this,"Error",Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<Peticion_Registro> call, Throwable t) {
                Toast.makeText(Registro.this, "Error",Toast.LENGTH_LONG).show();
            }
        });
    }
}