package com.example.alerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alerta.Api.Api;
import com.example.alerta.Viewmodels.Peticion_Login;
import com.example.alerta.Api.Service.ServicioPeticion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    TextView txtRegistro;
    EditText edtCorreo, edtPassword;
    Button btnSesion;
    Switch Sesion;
    String APITOKEN = "";
    int id;
    ServicioPeticion service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
        verificarPreferencias();
        edtCorreo = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtContra);
        btnSesion = findViewById(R.id.btnInicio);
        txtRegistro = findViewById(R.id.txtvRegistrarU);
        txtRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ints = new Intent(MainActivity.this, Registro.class);
                startActivity(ints);
            }
        });
        btnSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtCorreo.getText().toString().equals("") && edtPassword.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this,"Complete los campos",Toast.LENGTH_LONG).show();
                }else
                    Peticion(edtCorreo.getText().toString(), edtPassword.getText().toString());
            }
        });
    }

    private void Peticion(String correo, String password){
        service.Sesion(correo, password).enqueue(new Callback<Peticion_Login>() {
            @Override
            public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                if(response.isSuccessful()){
                    if(response.body().getEstado().equals(true)){
                        APITOKEN = response.body().getToken();
                        id = response.body().getId();
                        TokenTemporal();
                        startActivity(new Intent(MainActivity.this, Home.class));
                        finish();
                    }else
                        Toast.makeText(MainActivity.this,"Datos incorrectos",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Login> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Error inesperado",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void GuardarPreferencias(){
        String token = APITOKEN;
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("TOKEN", token);
        editor.putString("ID", ""+id);
        editor.commit();
    }

    private void verificarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if(token != ""){
            startActivity(new Intent(MainActivity.this, Home.class));
            finish();
        }
    }

    public void TokenTemporal(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("TOKEN_TEMP", APITOKEN);
        editor.putString("ID", ""+id);
        editor.commit();
    }
}
