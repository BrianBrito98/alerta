package com.example.alerta;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.alerta.Api.Api;
import com.example.alerta.Api.Service.ServicioPeticion;

public class Home extends AppCompatActivity {

    ServicioPeticion servicio;
    TextView txtTotalAlertas;
    Button btnAlertsG, btnAlertUser, btnVisuG, btnVisuUsuario, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verificarPreferencias();
        setContentView(R.layout.activity_home);
        servicio = Api.getApi(Home.this).create(ServicioPeticion.class);
        btnAlertsG = findViewById(R.id.btnAlertGeneral);
        btnAlertUser = findViewById(R.id.btnAlertUser);
        btnVisuG = findViewById(R.id.btnVisuGeneral);
        btnVisuUsuario = findViewById(R.id.btnVisuaUser);
        btnAlertUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Home.this, AlertasU.class));
            }
        });
        btnAlertsG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Home.this, Alertas.class));
            }
        });
        btnVisuG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Home.this, Visualizaciones.class));

            }
        });
        btnVisuUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Home.this, VisualizacionU.class));
            }
        });
    }
    private void verificarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        if(Api.token.equals("")){
            if(!preferencias.getString("TOKEN_TEMP", "").equals("") && preferencias.getString("TOKEN", "").equals("")){
                Api.token = preferencias.getString("TOKEN_TEMP", "");
            }else if(!preferencias.getString("TOKEN", "").equals("") && preferencias.getString("TOKEN_TEMP", "").equals("")){
                Api.token = preferencias.getString("TOKEN", "");
            }
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("TOKEN_TEMP", "");
        editor.putString("ID", "");
        editor.commit();
    }
}