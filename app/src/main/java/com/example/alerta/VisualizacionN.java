package com.example.alerta;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.alerta.Api.Api;
import com.example.alerta.Viewmodels.Peticion_CAlerta;
import com.example.alerta.Api.Service.ServicioPeticion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisualizacionN extends AppCompatActivity {

    ServicioPeticion servicio;
    Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizacion_noti);
        servicio = Api.getApi(VisualizacionN.this).create(ServicioPeticion.class);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            CrearVisualizacion(bundle.getString("ID"));
        }else{
            Toast.makeText(VisualizacionN.this, "Ocurrio Un error al crear la visualizacion", Toast.LENGTH_LONG).show();
            startActivity(new Intent(VisualizacionN.this, Home.class));
            finish();
        }
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VisualizacionN.this, Home.class));
                finish();
            }
        });
    }

    private void CrearVisualizacion(String IDAlerta){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio.CrearVisualziacion(preferences.getString("ID", ""), IDAlerta).enqueue(new Callback<Peticion_CAlerta>() {
            @Override
            public void onResponse(Call<Peticion_CAlerta> call, Response<Peticion_CAlerta> response) {
                if(!response.isSuccessful() || !response.body().isEstado()) {
                    Log.e("Error alerta", response.message());
                }else{
                    Toast.makeText(VisualizacionN.this, "Exito", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_CAlerta> call, Throwable t) {
                Toast.makeText(VisualizacionN.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}