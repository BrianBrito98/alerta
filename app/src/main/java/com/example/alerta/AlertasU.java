package com.example.alerta;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alerta.Api.Api;
import com.example.alerta.Viewmodels.AdapterA;
import com.example.alerta.Viewmodels.Peticion_Alertas;
import com.example.alerta.Api.Service.ServicioPeticion;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlertasU extends AppCompatActivity {

    ServicioPeticion servicio;
    RecyclerView rvAlertasU;
    AdapterA adapterA;
    Button btnRecargar;
    ImageButton imgbAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas_u);
        servicio = Api.getApi(AlertasU.this).create(ServicioPeticion.class);
        adapterA = new AdapterA();
        rvAlertasU = findViewById(R.id.rvAlertsU);
        rvAlertasU.setHasFixedSize(true);
        rvAlertasU.setLayoutManager(new LinearLayoutManager(AlertasU.this));
        rvAlertasU.addItemDecoration(new DividerItemDecoration(AlertasU.this, DividerItemDecoration.VERTICAL));
        AlertasUsuario();
    }
    private void AlertasUsuario(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio.AlertasU(preferences.getString("ID", "")).enqueue(new Callback<Peticion_Alertas>() {
            @Override
            public void onResponse(Call<Peticion_Alertas> call, Response<Peticion_Alertas> response) {
                if (response.isSuccessful()) {
                    List<Peticion_Alertas.detalle> alertas = response.body().getAlertas();
                    if(!alertas.isEmpty()){
                        adapterA.setData(alertas);
                        rvAlertasU.setAdapter(adapterA);
                    }else
                        Toast.makeText(AlertasU.this, "Alertas vacias", Toast.LENGTH_LONG).show();
                } else {
                    Log.e("Api Error: ", response.message() + " errorBody:" + response.errorBody());
                    Toast.makeText(AlertasU.this, "Error al procesar los datos", Toast.LENGTH_LONG).show();
                    AlertasUsuario();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Alertas> call, Throwable t) {
                Toast.makeText(AlertasU.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}