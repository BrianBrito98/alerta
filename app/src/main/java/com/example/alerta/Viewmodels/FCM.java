package com.example.alerta.Viewmodels;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.alerta.Api.Api;
import com.example.alerta.Api.Service.ServicioPeticion;
import com.example.alerta.R;
import com.example.alerta.VisualizacionN;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FCM extends FirebaseMessagingService {

    ServicioPeticion servicio;
    String Id;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e("token", "mi token es: "+s);
        guardarTokenNuevo(s);
    }

    public void guardarTokenNuevo(String s){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("token");
        ref.child("Usuario").setValue(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String from = remoteMessage.getFrom();
        Log.e("TAG","Mensaje Recibido de: "+from);
        if(remoteMessage.getNotification() != null){
            Log.e("TAG","Titulo: "+remoteMessage.getNotification().getTitle());
            Log.e("TAG","Body: "+remoteMessage.getNotification().getBody());
        }
        // -- Clave valor
        if(remoteMessage.getData().size() > 0){
            Log.e("TAG","Mi titulo es "+remoteMessage.getData().get("titulo"));
            Log.e("TAG","Mi detalle es "+remoteMessage.getData().get("detalle"));
            Log.e("TAG", "ID: "+ remoteMessage.getData().get("ID"));
            CrearAlerta();
            String titulo = remoteMessage.getData().get("titulo");
            String detalle = remoteMessage.getData().get("detalle");
            Id = remoteMessage.getData().get("ID");
            mayorQueOreo(titulo,detalle);
        }
    }

    private void mayorQueOreo(String titulo, String detalle){
        String id = "mensaje";
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, id);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel nc = new NotificationChannel(id, "nuevo", NotificationManager.IMPORTANCE_HIGH);
            nc.setShowBadge(true);
            assert  nm != null;
            nm.createNotificationChannel(nc);
        }
        builder.setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(titulo)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(detalle)
                .setContentIntent(clicnoti())
                .setContentInfo("nuevo");

        Random random = new Random();
        int idNotificacion = random.nextInt(8000);
        assert nm != null;
        nm.notify(idNotificacion, builder.build());
    }

    private PendingIntent clicnoti(){
        Intent it = new Intent(getApplicationContext(), VisualizacionN.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        it.putExtra("ID", Id);
        return PendingIntent.getActivity(this, 0, it, 0);
    }



    private void CrearAlerta(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio = Api.getApi(getApplicationContext()).create(ServicioPeticion.class);
        servicio.CrearAlerta(preferences.getString("ID", "")).enqueue(new Callback<Peticion_CAlerta>() {
            @Override
            public void onResponse(Call<Peticion_CAlerta> call, Response<Peticion_CAlerta> response) {
                if(!response.isSuccessful() || !response.body().isEstado())
                    Log.e("Error Alerta", "Ocurrio un error al crear una Alerta");
                else
                    Log.e("Exito Alerta", "Se a Creado Exitosamente una Alerta");
            }

            @Override
            public void onFailure(Call<Peticion_CAlerta> call, Throwable t) {
                Log.e("Error Conexion", t.getMessage());
                Log.e("Error Conexion", "Ocurrio un error al conectar con Firebase o al servidor");
            }
        });
    }
}
