package com.example.alerta.Viewmodels;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alerta.R;

import java.util.List;

public class AdapterA extends RecyclerView.Adapter<AdapterA.ViewHolder> {

    List<Peticion_Alertas.detalle> lista;
    private Context context;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alerts_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Peticion_Alertas.detalle alerta = lista.get(position);
        holder.txtvIdUsu.setText(""+ alerta.getUsuarioId());
        holder.txtvIdAlert.setText(""+ alerta.getId());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setData(List<Peticion_Alertas.detalle> lista){
        this.lista = lista;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtvIdUsu, txtvIdAlert;
        ImageView imageView;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtvIdUsu = itemView.findViewById(R.id.txtvIdUsu);
            txtvIdAlert = itemView.findViewById(R.id.txtvIdAlert);
        }
    }
}
